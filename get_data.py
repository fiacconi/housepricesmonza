#! /usr/bin/env python
# -*- coding: utf-8 -*-

import BeautifulSoup as bs
import urllib2
import sys
import warnings
import gc
import os


# ZONE LEGEND
# 1 = Via Liberta`, Cascine Bastoni, Sant'Albino
# 2 = San Rocco, Triante, Sant'Alessandro, San Giuseppe
# 3 = San Biagio, Cazzaniga
# 4 = San Fruttuoso, Taccona, Boscherona
# 5 = Buonarroti, Regina Pacis, Via San Damiano
# 6 = Centro
# 7 = Parco
# -99 = Else/not reported

# ENERGY CLASS LEGEND
# 0 = A+/A++/A+++
# 1 = A
# 2 = B
# 3 = C
# 4 = D
# 5 = E
# 6 = F
# 7 = G
# -99 = not reported

# STATUS LEGEND
# 1 = Nuovo / In costruzione
# 2 = Ottimo/Ristrutturato
# 3 = Buono / Abitabile
# 4 = Da ristrutturare
# -99 = not reported

def get_data(base_url, page_number):
    url = base_url+str(page_number)

    print "- Opening pag.", page_number
    web_page = urllib2.urlopen(url)

    print "  Scraping the page ..."
    web_page_soup = bs.BeautifulSoup(web_page)
    # announcement_list = web_page_soup.findAll("li", attrs={"class": "listing-item vetrina js-row-detail"})
    announcement_list = web_page_soup.findAll("div", attrs={"class": "listing-item_body--content"})

    print "  Announcements found:", len(announcement_list)

    db = []
    for announcement in announcement_list:

        if announcement.find("li", attrs={"class": "listing-features__price--private"}):
            continue

        if announcement.find("li", attrs={"class": "listing-features__price"}) is None:
            continue

        if "da" in announcement.find("li", attrs={"class": "listing-features__price"}).text:
            continue

        db.append({})
        # Extract web page for additional search
        db[-1]["url"] = announcement.find("p", attrs={"class": "titolo text-primary"}).find("a")["href"]

        # Extract price
        tmp = announcement.find("li", attrs={"class": "listing-features__price"})
        db[-1]["price"] = "" if (tmp is None) else tmp.text
        if db[-1]["price"].find(u"\u20ac") == -1:
            db[-1]["price"] = -99
        else:
            db[-1]["price"] = float(db[-1]["price"][db[-1]["price"].find(u"\u20ac") + len(u"\u20ac"):].replace(".", ""))

        # Extract surface
        tmp = announcement.find("li", attrs={"class": "listing-features__surface"})
        db[-1]["surface"] = "" if (tmp is None) else tmp.find("strong").text
        if db[-1]["surface"] != "":
            db[-1]["surface"] = float(db[-1]["surface"].replace(".", ""))
        else:
            db[-1]["surface"] = -99

        # Extract rooms
        tmp = announcement.find("li", attrs={"class": "listing-features__rooms"})
        db[-1]["rooms"] = "" if (tmp is None) else tmp.find("strong").text
        if db[-1]["rooms"] == "":
            db[-1]["rooms"] = 0.
        elif "5+" in db[-1]["rooms"]:
            db[-1]["rooms"] = 6.
        else:
            db[-1]["rooms"] = float(db[-1]["rooms"])

        # Extract zone
        title = announcement.find("p", attrs={"class": "titolo text-primary"}).find("a").text
        if (u"Via Libertà" in title) or (u"Cascine Bastoni" in title) or (u"Sant'Albino" in title):
            db[-1]["zone"] = 1.0
        elif (u"San Rocco" in title) or (u"Triante" in title) or (u"Sant'Alessandro" in title) or (u"San Giuseppe" in title):
            db[-1]["zone"] = 2.0
        elif (u"San Biagio" in title) or (u"Cazzaniga" in title):
            db[-1]["zone"] = 3.0
        elif (u"San Fruttuoso" in title) or (u"Taccona" in title) or (u"Boscherona" in title):
            db[-1]["zone"] = 4.0
        elif (u"Buonarroti" in title) or (u"Regina Pacis" in title) or (u"Via San Damiano" in title):
            db[-1]["zone"] = 5.0
        elif u"Centro" in title:
            db[-1]["zone"] = 6.0
        elif u"Parco" in title:
            db[-1]["zone"] = 7.0
        else:
            db[-1]["zone"] = -99.0

        # Get the additional data from house pages
        web_page_house = urllib2.urlopen(db[-1]["url"])
        web_page_house_soup = bs.BeautifulSoup(web_page_house)

        # Extract announcement date
        tmp = web_page_house_soup.first("dl", attrs={"class": "col-xs-12"})
        tmp = tmp.find("dd", attrs={"class": "col-xs-12 col-sm-7"}).text
        db[-1]["date_adv"] = str(tmp[tmp.rfind("-")+2:])

        # Extract construction year
        tmp = web_page_house_soup.findAll("dt")
        i = 0
        while i < len(tmp) and tmp[i].text != "Anno di costruzione":
            i += 1
        if i >= len(tmp):
            db[-1]["constr_yr"] = -99.0
        else:
            db[-1]["constr_yr"] = float(tmp[i].findNext("dd").text)

        # Extract energetic class
        i = 0
        while i < len(tmp) and not tmp[i].text in ["Classe energetica", "Certificazione energetica"]:
            i += 1
        if i >= len(tmp):
            db[-1]["energ_class"] = -99.0
        else:
            db[-1]["energ_class"] = tmp[i].findNext("dd").text

        if db[-1]["energ_class"] in ["A+", "A++", "A+++", "A++++", "A2", "A3", "A4"]:
            db[-1]["energ_class"] = 0.0
        elif db[-1]["energ_class"] == "A" or db[-1]["energ_class"] == "A1":
            db[-1]["energ_class"] = 1.0
        elif db[-1]["energ_class"] == "B":
            db[-1]["energ_class"] = 2.0
        elif db[-1]["energ_class"] == "C":
            db[-1]["energ_class"] = 3.0
        elif db[-1]["energ_class"] == "D":
            db[-1]["energ_class"] = 4.0
        elif db[-1]["energ_class"] == "E":
            db[-1]["energ_class"] = 5.0
        elif db[-1]["energ_class"] == "F":
            db[-1]["energ_class"] = 6.0
        elif db[-1]["energ_class"] == "G":
            db[-1]["energ_class"] = 7.0
        else:
            db[-1]["energ_class"] = -99.0

        # Extract status
        i = 0
        while i < len(tmp) and tmp[i].text != "Stato":
            i += 1
        if i >= len(tmp):
            db[-1]["status"] = -99.0
        else:
            db[-1]["status"] = tmp[i].findNext("dd").text

        if db[-1]["status"] == "Nuovo / In costruzione":
            db[-1]["status"] = 1.0
        elif db[-1]["status"] == "Ottimo/Ristrutturato":
            db[-1]["status"] = 2.0
        elif db[-1]["status"] == "Buono / Abitabile":
            db[-1]["status"] = 3.0
        elif db[-1]["status"] == "Da ristrutturare":
            db[-1]["status"] = 4.0
        else:
            db[-1]["status"] = -99.0

        # Extract energetic parameter
        i = 0
        while i < len(tmp) and "Indice di prestazione energetica globale" not in tmp[i].text:
            i += 1
        if i >= len(tmp):
            db[-1]["energ_index"] = -99.0
        else:
            db[-1]["energ_index"] = tmp[i].findNext("dd").text
            if db[-1]["energ_index"] == "" or "kWh/m" not in db[-1]["energ_index"]:
                db[-1]["energ_index"] = -99.0
            else:
                db[-1]["energ_index"] = db[-1]["energ_index"][:db[-1]["energ_index"].find("kWh/m")]
                if u"&#8805;" in db[-1]["energ_index"]:
                    db[-1]["energ_index"] = db[-1]["energ_index"][db[-1]["energ_index"].find(u"&#8805;")+len(u"&#8805;"):]
                db[-1]["energ_index"] = db[-1]["energ_index"].replace(".", "")
                db[-1]["energ_index"] = db[-1]["energ_index"].replace(",", ".")
                db[-1]["energ_index"] = float(db[-1]["energ_index"])

        web_page_house.close()
        del web_page_house_soup
        del tmp

    web_page.close()
    del announcement_list
    del web_page_soup
    gc.collect()

    return db


def dump_database(db, fout):
    print "  Dumping data"
    for i in range(len(db)):
        dic = db[i]
        fout.write("%s\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%s\n" % (dic["url"],
                                                             dic["price"],
                                                             dic["surface"],
                                                             dic["rooms"],
                                                             dic["zone"],
                                                             dic["constr_yr"],
                                                             dic["energ_class"],
                                                             dic["status"],
                                                             dic["energ_index"],
                                                             dic["date_adv"]))
    return


def usage():
    print ""
    print "./get_data.py -url   <base_url>"
    print "              -npg   <number_of_web_pages_to_scrap>"
    print "              -out   <ASCII_database_name=house_price_data.dat>"
    print ""
    exit()
    return


def parse_input_argument(argv):

    argc = len(argv)

    if argc == 1:
        usage()

    i = 1
    base_url = ""
    num_pages = 0
    output_db = "house_price_data.dat"

    while i < argc:
        if argv[i] in ["-h", "--help"]:
            usage()
        elif argv[i] == "-url":
            i += 1
            assert i < argc
            base_url = argv[i]
        elif argv[i] == "-npg":
            i += 1
            assert i < argc
            num_pages = int(argv[i])
        elif argv[i] == "-out":
            i += 1
            assert i < argc
            output_db = argv[i]
        else:
            warnings.warn("Option '%s' ignored!" % (argv[i]), category=RuntimeWarning)
        i += 1

    # Check the base_url
    if base_url == "":
        raise RuntimeError("Base url to scrap (from immobiliare.it) must be specified!")
    base_url = base_url[:base_url.find("pag=")+4]

    if num_pages == 0:
        raise RuntimeError("Number of web pages to scrap must be specified!")

    return base_url, num_pages, output_db


def main():

    base_url, num_web_pages, output_db = parse_input_argument(sys.argv)

    # Dump DB header
    if not os.path.isfile(output_db):
        fout = open(output_db, "w")

        fout.write("# ZONE LEGEND\n")
        fout.write("# 1 = Via Liberta`, Cascine Bastoni, Sant'Albino\n")
        fout.write("# 2 = San Rocco, Triante, Sant'Alessandro, San Giuseppe\n")
        fout.write("# 3 = San Biagio, Cazzaniga\n")
        fout.write("# 4 = San Fruttuoso, Taccona, Boscherona\n")
        fout.write("# 5 = Buonarroti, Regina Pacis, Via San Damiano\n")
        fout.write("# 6 = Centro\n")
        fout.write("# 7 = Centro\n")
        fout.write("# -99 = Else/not reported\n")
        fout.write("#\n")
        fout.write("# ENERGY CLASS LEGEND\n")
        fout.write("# 0 = A+/A++/A+++\n")
        fout.write("# 1 = A\n")
        fout.write("# 2 = B\n")
        fout.write("# 3 = C\n")
        fout.write("# 4 = D\n")
        fout.write("# 5 = E\n")
        fout.write("# 6 = F\n")
        fout.write("# 7 = G\n")
        fout.write("# -99 = not reported\n")
        fout.write("#\n")
        fout.write("# STATUS LEGEND\n")
        fout.write("# 1 = Nuovo / In costruzione\n")
        fout.write("# 2 = Ottimo/Ristrutturato\n")
        fout.write("# 3 = Buono / Abitabile\n")
        fout.write("# 4 = Da ristrutturare\n")
        fout.write("# -99 = not reported\n")
        fout.write("#\n")
        fout.write("# url\tprice\tsurface\trooms\tzone\tconstr_yr\tenerg_class\tstatus\tenerg_index\tdate_adv\n")
        fout.close()

    # Extract base data and additional data from each page
    for i in range(100, num_web_pages):
        db = get_data(base_url, i+1)
        fout = open(output_db, "a")
        dump_database(db, fout)
        fout.close()

    return


if __name__ == "__main__":
    main()
