#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import warnings
import numpy as np
import sklearn.model_selection as model_selection
import sklearn.linear_model as linear_model
import matplotlib.pyplot as pl
import matplotlib as mpl
from matplotlib import rcParams


def data_statistic(data_file):

    # Sample size
    db = np.genfromtxt(data_file)[:, 1:-1]

    n_sample = db.shape[0]
    db_complete = db[np.logical_and(np.logical_and(np.sum(db == -99.0, axis=1) == 0.0, db[:, 1] > 0.0), db[:, 2] > 0.0),
                  :]
    n_complete = db_complete.shape[0]

    print ""
    print " ============================================================== "
    print " - Total number of training examples: %d" % n_sample
    print " - Number of complete entries: %d / %d -- %.2f %%" % (n_complete, n_sample, 100. * n_complete / n_sample)

    # Sample size per zone
    print " ============================================================== "
    print " - Sample divided according to the 'Zone' feature"
    print "|  1  |  2  |  3  |  4  |  5  |  6  |  7  | -99 | 'Zone'"
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 8):
        sys.stdout.write("%4d |" % np.sum(db[:, 3] == i))
        sys.stdout.flush()
    sys.stdout.write("%4d |" % np.sum(db[:, 3] == -99))
    sys.stdout.write(" All data\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 8):
        sys.stdout.write("%4d |" % np.sum(db_complete[:, 3] == i))
        sys.stdout.flush()
    sys.stdout.write("%4d |" % np.sum(db_complete[:, 3] == -99))
    sys.stdout.write(" Complete data\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 8):
        sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 3] == i) / np.sum(db[:, 3] == i)))
        sys.stdout.flush()
    sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 3] == -99) / np.sum(db[:, 3] == -99)))
    sys.stdout.write(" Complete %\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 8):
        sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 3] == i) / len(db_complete[:, 3])))
        sys.stdout.flush()
    sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 3] == -99) / len(db_complete[:, 3])))
    sys.stdout.write(" Over total %\n")
    print "---------------------------------------------------------"
    print "1 = Via Liberta`, Cascine Bastoni, Sant'Albino"
    print "2 = San Rocco, Triante, Sant'Alessandro, San Giuseppe"
    print "3 = San Biagio, Cazzaniga"
    print "4 = San Fruttuoso, Taccona, Boscherona"
    print "5 = Buonarroti, Regina Pacis, Via San Damiano"
    print "6 = Parco"
    print "7 = Centro"
    print "-99 = Else/not reported"

    # Sample size per status
    print " ============================================================== "
    print " - Sample divided according to the 'Status' feature"
    print "|  1  |  2  |  3  |  4  | -99 | 'Status'"
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 5):
        sys.stdout.write("%4d |" % np.sum(db[:, 6] == i))
        sys.stdout.flush()
    sys.stdout.write("%4d |" % np.sum(db[:, 6] == -99))
    sys.stdout.write(" All data\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 5):
        sys.stdout.write("%4d |" % np.sum(db_complete[:, 6] == i))
        sys.stdout.flush()
    sys.stdout.write("%4d |" % np.sum(db_complete[:, 6] == -99))
    sys.stdout.write(" Complete data\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 5):
        sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 6] == i) / np.sum(db[:, 6] == i)))
        sys.stdout.flush()
    sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 6] == -99) / np.sum(db[:, 6] == -99)))
    sys.stdout.write(" Complete %\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 5):
        sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 6] == i) / len(db_complete[:, 6])))
        sys.stdout.flush()
    sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 6] == -99) / len(db_complete[:, 6])))
    sys.stdout.write(" Over total %\n")
    print "---------------------------------------------------------"
    print "1 = Nuovo / In costruzione"
    print "2 = Ottimo/Ristrutturato"
    print "3 = Buono / Abitabile"
    print "4 = Da ristrutturare"
    print "-99 = Else/not reported"

    # Sample size per number of rooms
    print " ============================================================== "
    print " - Sample divided according to the 'Number of rooms' feature"
    print "|  1  |  2  |  3  |  4  |  5  |  6+ | 'N. rooms'"
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 7):
        sys.stdout.write("%4d |" % np.sum(db[:, 2] == i))
        sys.stdout.flush()
    sys.stdout.write(" All data\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 7):
        sys.stdout.write("%4d |" % np.sum(db_complete[:, 2] == i))
        sys.stdout.flush()
    sys.stdout.write(" Complete data\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 7):
        sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 2] == i) / np.sum(db[:, 2] == i)))
        sys.stdout.flush()
    sys.stdout.write(" Complete %\n")
    print "---------------------------------------------------------"
    sys.stdout.write("|")
    for i in range(1, 7):
        sys.stdout.write("%4d |" % (100. * np.sum(db_complete[:, 2] == i) / len(db_complete[:, 2])))
        sys.stdout.flush()
    sys.stdout.write(" Over total %\n")
    print "---------------------------------------------------------"

    print ">>> ADJUST DATA <<<"
    print "1) Merge class 6 and 7 of the 'Zone' feature"
    db_complete[db_complete[:, 3] == 7, 3] = 6

    pr = np.zeros(6)
    for j in range(6):
        pr[j] = (db_complete[db_complete[:, 3] == j + 1, 0] / db_complete[db_complete[:, 3] == j + 1, 1]).mean()

    pr_class = pr.argsort() + 1
    new_class = np.zeros(db_complete[:, 3].size)
    for j in range(6):
        new_class[db_complete[:, 3] == j + 1] = pr_class[j]
    db_complete[:, 3] = new_class

    print " ============================================================== "

    np.savetxt("house_price_monza_clean.dat", db_complete)

    return db_complete


def analyse_data(db):

    # Features and target
    x = db[:, 1:]
    y = db[:, 0]

    # Create a new feature: gas bill per year, assuming a price of ~0.8 euro/standard cubic meter
    X = np.zeros((x.shape[0], x.shape[1]+1))
    X[:, :-1] = x
    X[:, -1] = X[:, 0] * X[:, -2] / 9.7 * 0.8

    # Correct the energy class feature for logarithm
    X[:, 4] += 1.0

    # Data cleaning, shift and normalization
    mean = np.log1p(X).mean(axis=0)
    std = np.log1p(X).std(axis=0)
    XX = (np.log1p(X) - np.log1p(X).mean(axis=0)) / np.log1p(X).std(axis=0)
    YY = (np.log1p(y) - np.log1p(y).mean()) / np.log1p(y).std()
    mean_y = np.log1p(y).mean()
    std_y = np.log1p(y).std()

    print " - Model to be tested"
    print "Surface, n. rooms, zone, constr_yr, energy class, status, energy index"
    print "---------------------------------------------------------"

    print " > Split dataset: 80% training - 20% test"
    # Split data in training and test sets
    X_train, X_test, y_train, y_test = model_selection.train_test_split(XX, YY, test_size=0.2, random_state=1988)
    print "---------------------------------------------------------"

    # Initialise linear regression object with cross-validation on the parameters
    linear_regression = linear_model.LassoCV(cv=5, alphas=np.logspace(-3., 1., 40), max_iter=10000)

    # Fit the model
    print " - Calculating model..."
    linear_regression.fit(X_train[:, 0:7], y_train)

    # Store values of the parameters chosen with cross-validation
    regularization_param = linear_regression.alpha_
    print " > Regularization parameter:", regularization_param
    print " > Coefficients:", linear_regression.coef_
    print " > Intercept:", linear_regression.intercept_

    # Test of the model on test set
    model_score = linear_regression.score(X_test[:, 0:7], y_test)
    print " ---> Model correlation coefficient = ", model_score
    print "---------------------------------------------------------"

    # PLOT RESULTS OF THE MODEL
    l = 5.0
    Lx = l + 1.5
    Ly = l + 1.2

    fig = pl.figure(figsize=(Lx, Ly))
    ax = fig.add_axes([1.3 / Lx, (Ly - 0.2 - l) / Ly, l / Lx, l / Ly])

    col = np.exp(X_test[:, 4] * std[4] + mean[4]) - 1.
    size = np.exp(X_test[:, 5] * std[5] + mean[5]) - 1

    ax.scatter(np.exp(X_test[:, 0] * std[0] + mean[0]) - 1,
               np.exp(std_y * linear_regression.predict(X_test[:, 0:7]) + mean_y) - 1,
               c=col, s=size / size.max() * 200, cmap="viridis", alpha=0.75)
    ax.plot(np.exp(X_test[:, 0] * std[0] + mean[0]) - 1, np.exp(std_y * y_test + mean_y) - 1, ".", color="r", ms=10,
            mec="none", alpha=0.5, label=r"\rm Test~Set")

    ax.text(30., 7.e5, r"$R^2 = %.3f$" % (np.around(model_score, 3)))

    lg = ax.legend(loc=1, numpoints=2, prop={"size": 20})

    ax_cb = fig.add_axes(
        [(1.3 + 0.1 * l) / Lx, (Ly - 0.2 - l + 0.85 * l) / Ly, 0.4 * l / Lx,
         0.05 * l / Ly])
    norm = mpl.colors.Normalize(vmin=col.min(), vmax=col.max())
    cb = mpl.colorbar.ColorbarBase(ax_cb, cmap="viridis", norm=norm, ticks=np.arange(1., 8.1, 1.0),
                                   orientation='horizontal')
    cb.set_label(r"$\rm En.~Class$", fontsize=14, labelpad=1)
    cb.set_ticklabels(
        [r"$\rm ^{+}A$", r"$\rm A$", r"$\rm B$", r"$\rm C$", r"$\rm D$", r"$\rm E$", r"$\rm F$", r"$\rm G$"])
    ax_cb.tick_params(labelsize=14)

    s = np.unique(size)
    lab_status = [r"$\rm New$", r"$\rm Opt$", r"$\rm Good$", r"$\rm Renov$"]
    ax.text(180.0, 7.0e4, r"$\rm Status$", fontsize=20)
    for j in range(4):
        ax.scatter(10. ** (np.log10(100) + j * (np.log10(500) - np.log10(100)) / 3), 5.0e4,
                   s=200 * s[j] / s.max(), c="grey")
        ax.text(10. ** (np.log10(100) + j * (np.log10(500) - np.log10(100)) / 3), 3.0e4, lab_status[j],
                fontsize=14, ha="center")

    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim([20, 800])
    ax.set_ylim([2e4, 6e6])
    ax.set_xticks([50, 100, 200, 500])
    ax.set_yticks([50000, 100000, 200000, 1000000])
    ax.set_xlabel(r"${\rm Surface~(m^2)}$", labelpad=15)
    ax.set_xticklabels([r"$50$", r"$100$", r"$200$", r"$500$"])
    ax.set_ylabel(r"${\rm Price~(Euro)}$", labelpad=15)
    ax.set_yticklabels([r"$50{\rm k}$", r"$100{\rm k}$", r"$200{\rm k}$", r"$1{\rm M}$"])

    fig.savefig("img/model.png", dpi=150)
    pl.show()

    # RESIDUALS
    l = 5.0
    Lx = 1.5*l + 1.4
    Ly = l + 1.2

    fig = pl.figure(figsize=(Lx, Ly))
    ax = fig.add_axes([1.2 / Lx, (Ly - 0.2 - l) / Ly, 1.5*l / Lx, l / Ly])

    res = np.log10(np.exp(std_y * linear_regression.predict(X_test[:, 0:7]) + mean_y) - 1.) - np.log10(
        np.exp(std_y * y_test + mean_y) - 1.)

    mu = res.mean()
    sigma = res.std()

    print "> Mean residual of Log Price = ", mu, " correspond to Pred/True =", 10.**mu
    print "> Standard deviation of residual of Log Price = ", sigma

    h, b = np.histogram(res, range=[-0.4, 0.4], bins=20)
    h = h / (h * np.diff(b)).sum()

    x = np.linspace(-0.5, 0.5, 100)
    y = np.exp(-0.5 * (x-mu)**2 / sigma**2) / (2 * np.pi)**0.5 / sigma

    for i in range(len(h)):
        ax.fill_between([b[i], b[i+1]], h[i], 0.0, color="#99bbff")

    ax.plot(b[1:], h, "-b", drawstyle="steps-pre", lw=2)
    ax.plot(x, y, "--r", lw=4)

    ax.text(-0.35, 3.5, r"$\mu_{R} = %.3f$"%(np.around(mu, 3)))
    ax.text(-0.35, 3.0, r"$\sigma_{R} = %.3f$" % (np.around(sigma, 3)))

    ax.minorticks_on()
    ax.set_xlim([-0.4, 0.4])
    ax.set_ylim([0.0, 4])
    ax.set_xlabel(r"${\rm RES} \equiv \log_{10}({\rm Pred.~Price}) - \log_{10}({\rm Real~Price})$", labelpad=15)
    ax.set_ylabel(r"${\rm d} \mathcal{P}/ {\rm d} {\rm RES}$", labelpad=15)

    fig.savefig("img/residuals.png", dpi=150)
    pl.show()

    print "---------------------------------------------------------"

    # LEARNING CURVES
    l = 5.0
    Lx = 1.5*l + 1.4
    Ly = l + 1.2

    fig = pl.figure(figsize=(Lx, Ly))
    ax = fig.add_axes([1.1 / Lx, (Ly - 0.2 - l) / Ly, 1.5*l / Lx, l / Ly])

    train_size, train_score, cv_score = model_selection.learning_curve(
        linear_model.Lasso(alpha=regularization_param, max_iter=10000), X_train[:, 0:7], y_train,
        train_sizes=np.linspace(0.1, 1.0, 9), shuffle=True, cv=5, scoring="r2")

    ax.fill_between(train_size, train_score.mean(axis=1) + train_score.std(axis=1),
                    train_score.mean(axis=1) - train_score.std(axis=1), color="b", alpha=0.5)
    ax.plot(train_size, train_score.mean(axis=1), "-ob", ms=8, mec="b", mfc="b", lw=2, label=r"$\rm Training~score$")

    ax.fill_between(train_size, cv_score.mean(axis=1) + cv_score.std(axis=1),
                    cv_score.mean(axis=1) - cv_score.std(axis=1), color="r", alpha=0.5)
    ax.plot(train_size, cv_score.mean(axis=1), "--or", ms=8, mec="r", mfc="r", lw=2,
            label=r"$\rm Cross\mbox{-}Validation~score$")

    lg = ax.legend(loc=4, handlelength=3)
    lg.draw_frame(False)

    ax.minorticks_on()
    ax.set_xlim([100, 1000])
    ax.set_ylim([0.6, 1])
    ax.set_xlabel(r"$\rm Training~examples$")
    ax.set_ylabel(r"$R^2~{\rm correlation~score}$")

    fig.savefig("img/learning_curves.png", dpi=150)
    pl.show()

    return


def usage():
    print ""
    print "./analyse_data.py -d <data_file>"
    print ""
    exit()
    return


def parse_arguments(argv):
    argc = len(argv)

    data_file = ""

    if argc == 1:
        usage()

    i = 1
    while i < argc:
        if argv[i] in ["-h", "--help"]:
            usage()
        if argv[i] == "-d":
            i += 1
            assert i < argc
            data_file = argv[i]
        else:
            warnings.warn("Option '%s' ignored!" % (argv[i]), category=RuntimeWarning)
        i += 1

    if data_file == "":
        raise RuntimeError("Data file must be specified!")

    return data_file


def main():

    data_file = parse_arguments(sys.argv)

    db_complete = data_statistic(data_file)
    analyse_data(db_complete)

    return

if __name__ == "__main__":
    rcParams['text.usetex'] = True
    rcParams['text.latex.unicode'] = True
    rcParams['font.size'] = 22.0
    rcParams['xtick.major.size'] = 10
    rcParams['xtick.minor.size'] = 5
    rcParams['ytick.major.size'] = 10
    rcParams['ytick.minor.size'] = 5
    main()
