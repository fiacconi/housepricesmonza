# House Prices in Monza, Italy

This is a simple data-science project to analyse the trends in the house prices in Monza, a town north-east of Milan.   
The project is divided in three phases: data collection, data preprocessing, and data analysis.

## Data collection

Data have been collected from the website [Immobiliare.it](http://www.immobiliare.it) which reports house selling advertisements.
I used the Python package [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/) to scrape the web pages and to extract the feautures and the price for each advertisement.
Specifically, I first get some general pieces of information (e.g. surface and number of rooms) from the summary page, while some additional features (e.g. the energy class or the status of the house) are extracted from the web page of each advertisement.

The code to collect the data is contained in the file ```get_data.py```.
I collected the data on the 13th of January 2018.
I collected raw data for 2450 advertisements that were still available on the website by that date; they have been announced between the early January 2018 and June 2016.
I selected only the advertisements announced until the end of 2017, for a total of 2377 records dumped in the file ```house_price_monza.dat```.
Each raw entry contains information for the following features, which represent the basic information available to the buyers.

- House price (in Euros)
- Surface (in square meters)
- Number of rooms (natural number, from 1 to 6, 6 refers to 6 or more rooms)
- Zone (the different zones are available in the header of ```get_data.py```)
- Contruction year
- Energy class
- House status
- Global energy index (in kWh / sqm / yr)

Some of this quantities (house price, surface, global energy index) are real numbers, some are integer numbers (number of rooms, contruction year), while some of them are discrete properties that can be ordered.
Specifically, we have:

1. the town is divide by the website in 7 distinct zones that I labelled from 1 to 7;
2. the energy class goes from A+ (or better) to G, labelled from 0 to 7, i.e. from the most to the least energy efficient;
3. the house status can be New/Excellent/Good/To be renewed, labelled from 0 to 3, i.e. from the best to the worst status.

There is also the url of the individual web page and the announcement date.

## Data preprocessing

We clean the data by means of function ```data_statistic``` contained in file ```analyse_data.py```.
First, I selected the only the 1466 entries for which all features and prices are available.
Then I checked that those are roughly evenly distributed among the feature with discrete nature, such as the zone or the status.
I cured in particular the zone feature: I merged together class 6 and 7, then I relabelled each zone to be ordered according to the average price/surface of the houses in each zone, going from 1, i.e. the cheepest area, to 6, i.e. the most expensive area.
The resulting dataset is dumped in the file ```house_price_monza_clean.dat```.
Before starting to analyse the data, I also transformed all quantities as X -> log(1+X) to reduce the skewness of some features, and I shifted and normalised all features/prices by their mean and standard deviation, respectively (at the beginning of function ```analyse_data``` in file ```analyse_data.py```).

## Data analysis and results

The analysis of the data is performed in function ```analyse_data``` in file ```analyse_data.py``` and is based on routines provided by the Python package [scikit-learn](http://scikit-learn.org/stable/).
I used a linear regression model to describe the house price as a function of all the other features (I included both the energy class and the global energy index; despite their information is redundant, picking one of the two has very minor effects on the following results).

First, I divided the dataset in training (80%) and test set (20%).
Then I trained a linear regression model with L1 penalty on the parameters.
The optimisation parameter is selected by dividing the original training set in a training and a cross-validation set.
The performance of the model is then evaluated on the test set, which provides a R^2 score of about 0.86.

The figure below shows the results of the modelling comparing the predictions on the test set with the test set itself.
The optimization requires a 0 coefficient for the contruction year, i.e. it does not affect the price, while the largest coefficients (in magnitude) are for the surface (as expected), the status, the zone, and the energetic class.
The coefficient signs are all as expected.

![Model results](img/model.png)

We can conclude that:

- The house price correlate mostly with the house surface.
- Newer or better mantained houses are more expensive.
- Energy efficient houses are more expensive than inefficient ones.
- The price depends somewhat on the zone, though with large scattering.

The figure below shows the distribution of the residuals calculated as the difference in the Log10 of the predicted and observed house prices in the test set.
The residuals follow a Gaussian distribution, and I overplotted the Gaussian with mean and standard deviation as extracted from the data. The mean square error is as small as ~0.1.

![Residuals of the model](img/residuals.png)

Finally, I show the learning curves (R^2 score as a function of the training sample size) associated with the model using a 5-fold cross-validation set.
The cross-validation and the training scores reach very similar average values of ~0.8, suggesting that the model is unlikely to be severely affected by a variance problem.
Despite 0.8 is a good score, this may possibly suggest that the model might be somewhat affected by some bias problem.
Such problem could be alleviated by collecting more features to increase the model complexity.

![Learning curves](img/learning_curves.png)
